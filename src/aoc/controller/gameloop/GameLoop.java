package aoc.controller.gameloop;

import java.util.List;
import java.util.Optional;
import aoc.controller.Controller;
import aoc.controller.GameConstants;
import aoc.controller.datamanager.DataManager;
import aoc.model.Model.GameStatus;
import aoc.model.Model;
import aoc.model.Model.ShootingStyle;
import aoc.model.ModelImpl;
import aoc.model.entity.EntityInterface;
import javafx.application.Platform;
import aoc.utilities.Direction;
import aoc.view.gameview.GameSceneRender;
import aoc.utilities.Images;

/**
 * This class represents the gameloop of the game. It coordinates the update of the model, the handling of the inputs and the render of the model status.
 * It also recognis if the game is won or lost, and ensures that the correct balance between fps and ups is maintained.
 */
public class GameLoop extends Thread implements GameLoopInterface {

    /**
     * This enumeration lists all the possible statuses of the gameloop.
     */
    public enum Status {

	/**
	 * The GameLoop is running.
	 */
	RUNNING,
	
	/**
	 * The GameLoop is paused.
	 */
	PAUSED,
	
	/**
	 * The GameLoop is ready to run.
	 */
	READY,
	
	/**
	 * The GameLoop has been killed (can't run anymore).
	 */
	KILLED;
    }

    private static final double TIME_PER_UPDATE = 1000 / GameConstants.UPS;

    private static final double UPDATES_PER_FRAME = GameConstants.UPS / Controller.get().getFPS();

    private Status status;

    private final Model model;

    private long updateTime;

    private final GameSceneRender gameview;

    /**
     * The constructor of the gameloop.
     * @param index
     *          the index of the level of the story that this gameloop shall execute.
     * @param gameview
     *          the reference of the gameview associated to this gameloop.
     */
    public GameLoop(final Optional<Integer> index, final GameSceneRender gameview) {
    	super();
    	this.model = new ModelImpl(index);
    	this.status = Status.READY;
    	this.gameview = gameview;
    	gameview.setLabel(index.get());
    	this.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void end() {
       	this.status = Status.KILLED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void pause() throws IllegalStateException {
      	this.checkCondition(this.status == Status.RUNNING, "The GameLoop is not running");
      	this.status = Status.PAUSED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void proceed() throws IllegalStateException {
    	this.checkCondition(this.status == Status.PAUSED, "The GameLoop is not paused");
    	this.status = Status.RUNNING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Status getStatus() {
    	return this.status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
    	this.status = Status.RUNNING;
    	int counter = 0;
    	while (this.getStatus() == Status.RUNNING || this.getStatus() == Status.PAUSED) {
    		this.updateTime = System.currentTimeMillis();
    		this.manageInputs();
    		if (this.getStatus() != Status.PAUSED) {
    			this.update();
    			counter++;
    			if (counter >= UPDATES_PER_FRAME) {
    			    counter = 0;
    			    this.render(model.getEntityList());
    			}
    			this.waitNextUpdate();
    		}
    	}
    	try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getLevel() {
	return Optional.of(model.getCurrentLevel());
    }

    /**
     * This method ensures that the update lasts at least the amount of time defined by TIME_PER_UPDATE.
     */
    protected void waitNextUpdate() {
    	final long time = System.currentTimeMillis() - this.updateTime;
    	if (time < GameLoop.TIME_PER_UPDATE) {
    		try {
				Thread.sleep((long) (GameLoop.TIME_PER_UPDATE - time));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    }

    /**
     * This method renders the actual status of the model. It launches the render operation in another thread.
     * @param entities
     *          the list of the entities to be rendered.
     */
    protected void render(final List<EntityInterface> entities) {
        final Runnable render = () -> {
            gameview.init();
            entities.forEach(e -> {
                switch (e.getName()) {
                    case "MOTHER":
                        gameview.draw(Images.MOTHER.getImage(), e.getPosition());
                        break;
                    case "DUMB_CHILD":
                        gameview.draw(Images.DUMB_CHILD.getImage(), e.getPosition());
                        break;
                    case "FAT_CHILD":
                        gameview.draw(Images.FAT_CHILD.getImage(), e.getPosition());
                        break;
                    case "NERD_CHILD":
                        gameview.draw(Images.NERD_CHILD.getImage(), e.getPosition());
                        break;
                    case "ATHLETIC_CHILD":
                        gameview.draw(Images.ATHLETIC_CHILD.getImage(), e.getPosition());
                        break;
                    case "RICH_CHILD":
                        gameview.draw(Images.RICH_CHILD.getImage(), e.getPosition());
                        break;
                    case "BASIC_SLIPPER":
                        gameview.draw(Images.SLIPPER.getImage(), e.getPosition());
                        break;
                    default:
                        break;
                }
            });
    	};
    	Platform.runLater(render);
    }

    /**
     * This method updates the status of the model and checks if the  match is won or lost.
     */
    protected void update() {
    	this.model.update();
    	if (this.model.getGameStatus() != GameStatus.PLAYING) {
    	    if (this.model.getGameStatus() == GameStatus.WON) {
    	        if (DataManager.getDataManager().getProgress() == this.model.getCurrentLevel()
    	                && this.model.getCurrentLevel() < GameConstants.N_LEVELS) {
    	                DataManager.getDataManager().updateProgress();
    	        }
    	        Platform.runLater(() -> gameview.win());
    	    } else if (this.model.getGameStatus() == GameStatus.LOST) {
    	    Platform.runLater(() -> gameview.lost());
    	    }
    	this.end();
    	}
    }

    /**
     * This method manages the input collected by the view.
     */
    protected void manageInputs() {
	this.gameview.getInput().forEach(input -> {
	    switch (input) {
	        case UP:
	            this.model.moveMother(Direction.UP);
	            break;
	        case DOWN:
	            this.model.moveMother(Direction.DOWN);
	            break;
	        case SHOT:
	            this.model.shoot(ShootingStyle.SINGLE);
	            break;
	        case RAPID_SHOT:
	            this.model.shoot(ShootingStyle.RAPID);
	            break;
	        case PAUSE:
	            Platform.runLater(() -> gameview.pause());
	            break;
	        default:
	            break;
	    }
	});
    }

    /**
     * This method check if a condition is true;
     * if not it throws an Exception with the message passed as argument.
     * @param supplier
     *          the condition to check.
     * @param message
     *          the String of the eventual error
     * @throws IllegalArgumentException
     *          It is thrown in case the supplier is false.
     */
    private void checkCondition(final boolean supplier, final String message) throws IllegalStateException {
    	if (!supplier) {
    		throw new IllegalArgumentException(message);
    	}
    }
}
