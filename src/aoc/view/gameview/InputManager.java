package aoc.view.gameview;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import aoc.utilities.Input;

/**
 * This class provides methods to collect
 * and return the Inputs that the game view receives.
 */
public final class InputManager {

    private List<Input> inputList = new LinkedList<>();
    private List<Input> buffer = new LinkedList<>();


    /**
     * It contains the SINGLETON, initialized at first use.
     */
    private static class LazyHolder {
        /**
         * Contains the reference to the Singleton.
         */
        private static final InputManager SINGLETON = new InputManager();
    }

    private InputManager() {

    }

    /**
     * This method returns the list of inputs.
     * @return list of inputs
     */
    public List<Input> getInputs() {
	return Collections.unmodifiableList(this.inputList);
    }

    /**
     * This method clears the buffer,
     * loading its content in inputList.
     */
    public synchronized void clearBuffer() {
	this.inputList = new LinkedList<Input>(this.buffer);
	this.buffer.clear();
    }

    /**
     * This method adds an input to the buffer.
     * @param input
     *          the input given by the user
     */
    public synchronized void addInput(final Input input) {
	this.buffer.add(input);
    }

    /**
     * Method that returns this class.
     * @return an instance of this class
     */
    public static InputManager getSingleton() {
        return LazyHolder.SINGLETON;
    }
}
