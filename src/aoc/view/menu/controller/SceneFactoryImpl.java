package aoc.view.menu.controller;

import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * The class that produces a scene that has the desired FXML.
 *
 */
public class SceneFactoryImpl implements SceneFactory {

	private Optional<Scene> scene = Optional.empty();
	
	@Override
    public final void setScene(final String nameFXML) throws IOException {
	    Parent parent = FXMLLoader.load(getClass().getResource(getPathFXML(nameFXML)));
	    this.scene = Optional.of(new Scene(parent, Utils.STAGE_HEIGHT, Utils.STAGE_WIDTH));
	    scene.get().getStylesheets().add(getClass().getResource(getPathFXML("style.css")).toExternalForm());
	}

	@Override
    public final Scene getScene() {
	    try {
	        checkCondition(scene.isPresent(), "No scene is currently setted");
	        return this.scene.get();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
        return null;
	}	
	
	private String getPathFXML(final String fxmlName) {
		return GamePaths.FXML_PATH + fxmlName;
	}

	/**
        * This method check if a condition is true;
        * if not it throws an Exception with the message passed as argument.
        * 
        * @param supplier
        * @param message
        * @throws IllegalStateException
        */
        private void checkCondition(final boolean supplier, final String message) throws IllegalStateException {
            if (!supplier) {
                throw new IllegalStateException(message);
            }
        }
}
