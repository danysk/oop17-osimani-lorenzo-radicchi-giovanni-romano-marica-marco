package aoc.view.menu.controller;

import java.net.URL;
import java.util.ResourceBundle;

import aoc.controller.Controller;
import aoc.controller.GameConstants;
import aoc.controller.datamanager.DataManager;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;

/**
 * The controller class for the Option scene.
 *
 */
public class OptionController extends BasicController implements Initializable {

	private static final ObservableList<Integer> list = 
	        FXCollections.observableArrayList(GameConstants.FPS_12,GameConstants.FPS_30, GameConstants.FPS_60);
	
	@FXML
	CheckBox check;
	
	@FXML
	ChoiceBox<Integer> combo;
	
	@FXML
	Slider slider;

	/**
	 * To delete all progress in game.
	 * 
	 * @param event
	 *         the click 
	 */
	@FXML
	private void deleteAction(final ActionEvent event) {
		DataManager.getDataManager().eraseData();
	}
	
	/**
	 * To set the selected FPS in the ChoicheBox.
	 * 
	 * @param event
	 *         the FPS selected
	 */
	@FXML
	private void setFPS(final ActionEvent event) {
		Controller.get().setFPS(combo.getValue());
	}

	@Override
    public final void initialize(final URL location, final ResourceBundle resources) {
		combo.setItems(list);
		combo.setValue(list.get(1));
		slider.setValue(MainWindow.getSingleton().getSoundManager().getVolume() * 100);
		slider.valueProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(final Observable observable) {
			    MainWindow.getSingleton().getSoundManager().setVolume(slider.getValue() / 100);
			}
		});
	}
}
