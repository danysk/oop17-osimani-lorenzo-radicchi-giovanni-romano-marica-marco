package aoc.view.menu.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import aoc.controller.Controller;
import aoc.controller.datamanager.DataManager;
import aoc.view.gameview.AbstractGameView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * The controller class for the SelectStage scene.
 *
 */
public class SelectStageController extends BasicController implements Initializable {

	private static final List<Button> buttonList = new ArrayList<>();
	
	@FXML
	Button level1;
	@FXML
	Button level2;
	@FXML
	Button level3;
	@FXML
	Button level4;
	@FXML
	Button level5;

	/**
	 * Start game in the selected level.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void playGame(final ActionEvent event) throws IOException {
		Button button = (Button) event.getSource();
		String name = button.getText();
		MainWindow.getSingleton().getSoundManager().clickPlay();
		MainWindow.getSingleton().getFactory().setScene("GameViewStory.fxml");
	        Scene scene = MainWindow.getSingleton().getFactory().getScene();
	        scene.setOnKeyPressed(AbstractGameView.inputHandler);
	        scene.setOnKeyReleased(AbstractGameView.turboHandler);
		Optional<Integer> index = Optional.empty();
		switch (name) { 
		  case "Level 1": 
		      index = Optional.of(1);
		  break; 

		  case "Level 2": 
		      index = Optional.of(2);
		  break; 

		  case "Level 3": 
		      index = Optional.of(3);
		  break; 

		  case "Level 4":
		      index = Optional.of(4);
		  break;

		  case "Level 5":
		      index = Optional.of(5);
		  break;
		  default:
		  break;
		}

		Controller.get().start(index);
	        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
	        window.setScene(scene);
	        window.show();
	}
	
	/**
	 * Set the MainMenu scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@Override
	public void initialize(final URL location, final ResourceBundle resources) {
		buttonList.add(level1);
		buttonList.add(level2);
		buttonList.add(level3);
		buttonList.add(level4);
		buttonList.add(level5);

		for (int i = buttonList.size() - 1; i >= DataManager.getDataManager().getProgress(); i--) {
			buttonList.get(i).setDisable(true);
		}
		buttonList.clear();
	}

}
